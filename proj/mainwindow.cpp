#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), ui(new Ui::MainWindow)
{
    qsrand(QDateTime::currentMSecsSinceEpoch());


    ui->setupUi(this);
    this->resize(600, 500);

    m_sortLabel = new QLabel("Sorting: ", this);
    m_sortLabel->setGeometry(15, 15, 150, 25);
    m_arraySizeLabel = new QLabel("Array size: ", this);
    m_arraySizeLabel->setGeometry(325, 15, 150, 25);
    m_timeIntervalLabel = new QLabel("Time interval: ", this);
    m_timeIntervalLabel->setGeometry(485, 15, 150, 25);

    m_sort = new QComboBox(this);
    m_sort->setGeometry(15, 45, 150, 25);
    m_arraySize = new QLineEdit("20", this);
    m_arraySize->setGeometry(325, 45, 100, 25);
    m_timeInterval = new QLineEdit("100", this);
    m_timeInterval->setGeometry(485, 45, 100, 25);

    m_scene = new QGraphicsScene(this);
    m_scene->setSceneRect(0, 0, 570, 330);

    m_view = new QGraphicsView(this);
    m_view->setScene(m_scene);
    m_view->setGeometry(15, 100, 572, 332);

    m_generateNewArray = new QPushButton("Generate new array", this);
    m_generateNewArray->setGeometry(150, 460, 130, 25);
    m_start = new QPushButton("Start", this);
    m_start->setGeometry(320, 460, 130, 25);

    m_brush = QBrush(QColor(qrand() % 255, qrand() % 255, qrand() % 255));
    m_pen = QPen(QColor(qrand() % 255, qrand() % 255, qrand() % 255));

    generate();
    draw();

    connect(m_generateNewArray, &QPushButton::clicked, this, &MainWindow::generate);
    connect(m_generateNewArray, &QPushButton::clicked, this, &MainWindow::draw);
    connect(m_start, &QPushButton::clicked, this, &MainWindow::start);
    connect(this, &MainWindow::drawSignal, this, &MainWindow::draw);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::generate()
{
    m_array.clear();

    int size = m_arraySize->text().toInt();

    for (int i = 0; i < size; i++)
        m_array.push_back(qrand() % 100);
}

void MainWindow::draw()
{
    m_scene->clear();

    int max = m_array[0];
    for(int i = 0; i < m_array.count(); i++)
        if(m_array[i] > max)
            max = m_array[i];

    int k = (m_scene->height() - 5) / max;
    int rectWidth = m_scene->width() / (1.5 * m_array.count());
    for(int i = 0; i < m_array.count(); i++)
            m_scene->addPolygon(QPolygon(QRect(1.5 * i * rectWidth + rectWidth + 5, m_scene->height() - 5,
                                               -rectWidth, -k * m_array.at(i))), m_pen, m_brush);
}

void MainWindow::start()
{
    bool continueFlag = false;
    int timeInterval = m_timeInterval->text().toInt();

    do
    {
        continueFlag = false;

        for (int i = 0; i < m_array.count() - 1; i++)
        {
            emit drawSignal();
            if (m_array[i] > m_array[i + 1])
            {
                std::swap(m_array[i], m_array[i + 1]);
                continueFlag = true;
                QEventLoop loop; QTimer::singleShot(timeInterval, &loop, SLOT(quit())); loop.exec();
            }
        }
    } while(continueFlag);
}
