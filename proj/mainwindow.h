#include <QMainWindow>
#include<QVector>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPushButton>
#include <QLineEdit>
#include <QComboBox>
#include <QLabel>
#include <QStringList>
#include <QDateTime>
#include <QTimer>
#include <QDebug>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void drawSignal();

public slots:
    void generate();
    void draw();
    void start();

private:
    Ui::MainWindow *ui;
    QGraphicsScene *m_scene;
    QGraphicsView *m_view;
    QLabel *m_sortLabel;
    QLabel *m_arraySizeLabel;
    QLabel *m_timeIntervalLabel;
    QComboBox *m_sort;
    QLineEdit *m_arraySize;
    QLineEdit *m_timeInterval;
    QPushButton *m_generateNewArray;
    QPushButton *m_start;
    QBrush m_brush;
    QPen m_pen;
    QVector<int> m_array;
};
